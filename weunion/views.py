from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response, get_object_or_404,redirect
from defects.models import Town
from .helper import Towns
import urllib.request, json
from .models import Regions, User
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import PermissionDenied


def index(request,slug = None):
    if(slug):
         request.session["town"] = Town.objects.get(slug=slug).id
         request.session["town_name"]= Town.objects.get(slug=slug).name

    if("town" not in request.session and "town_name" not in request.session):
        if(request.user.is_authenticated()):
             request.session["town"] = request.user.towns.all()[0].id
             request.session["town_name"] = request.user.towns.all()[0].name

    if("town" in request.session and "town_name" in request.session):
        issues = None
        petitions = None
        news = None
        allowed = None
        town = get_object_or_404(Town,pk=request.session["town"])
        town_banners = town.townbanners_set.all()
        modules = [m.module.id for m in Town.objects.get(pk=request.session["town"]).townallowedmodules_set.all()]
        if(1 in modules):# Дефекты ЖКХ
            from defects.models import Issues
            issues = [i for i in Issues.objects.filter(parent_task_ref=None, town_ref=request.session["town"]).order_by('-id')[:3] if i.last_issue().status !=0 and i.last_issue().status !=3]

        if(2 in modules):#Петиции
            from petitions.models import Petitions
            petitions = Petitions.objects.filter(town = request.session["town"], status = 2).order_by('-id')[:3]

        if(3 in modules):#Новости
            from news.models import News

            news = News.objects.filter(town=request.session["town"], publish=1).order_by('-id')[:3]

        return render(request, 'summarypage.html',{'issues': issues, 'petitions': petitions, 'news': news, 'town': town,'town_banners': town_banners})

    else:
        return render(request, 'guestpage.html')


def test(request):

    return render(request,'test.html')

#вывести список городов согласно области
def towns(request, region_ref):
    region = get_object_or_404(Regions, id=region_ref)
    tws = Town.objects.filter(region_ref = region).all()
    return render(request, 'towns.html', {'towns': tws})

#the ABOUT page
def about(request):
    return render(request, 'about.html')

#the CONTACTS page
def contacts(request):
    return render(request, 'contacts.html')

#the HELP page
def help(request):
    return render(request, 'help.html')

#the JOIN page
def join(request):
    return render(request, 'join.html')

#the JOIN page
def rules(request):
    return render(request, 'rules_common.html')

#Выбор города с помещением в сессию
def choosetown(request, townid):
    town_name = get_object_or_404(Town, id=townid).name
    slug = get_object_or_404(Town, id=townid).slug
    response = redirect('/town/'+slug)
    #response.set_cookie('town',townid)
    #response.set_cookie('town_name', town_name)
    request.session["town"]=townid
    request.session["town_name"]=town_name
    return response

#Формируем название города в хэддере
def gettown(request):
     if("town" in request.session and "town_name" in request.session):
        return HttpResponse(request.session["town_name"])
     elif(request.user.is_authenticated()):
         town_name = request.user.towns.all()[0].name
         townid = request.user.towns.all()[0].id
         response =  HttpResponse(town_name)
         request.session["town"]=townid
         request.session["town_name"]=town_name
         return response
     else:
         return HttpResponse(False)

def _get_town_name(town_id):
    '''Возвращаем Имя города'''
    if(town_id):
         town_name = Town.objects.get(id=town_id).name
         return town_name
    else:
         return 'Вкажіть ID міста'

#страница регионов
def regions(request):
    return render(request,'regions.html')

#сраничка IGOV
def igov(request):
    if("town" in request.session and "town_name" in request.session):
        if(request.is_ajax()):
            req=urllib.request.Request("https://igov.org.ua/api/catalog?%s" % Town.objects.get(pk=request.session["town"]).additions.get(type = 'igov').body)
            res=urllib.request.urlopen(req)
            data=res.read().decode("UTF-8")
            data=json.loads(data)
            return render_to_response('_igov-ajax.html',{"sData":data})
        else:
            return render(request,'igov.html')
    else:
        return redirect('regions')

#сраничка Open Budget
def budget(request):
    if("town" in request.session):
            openbudget = Town.objects.get(pk = request.session['town']).openbudget.all()[0].body
            return render(request,'budget.html',{"openbudget": openbudget})
    else:
        redirect('regions')


def prozorro(request):
    """Вытягиваем данные из системы Прозоро. Пока для e-tender"""
    if("town" in request.session):
        return render(request, 'prozorro.html')
    else:
        return redirect('regions')


def profile(request,user_id):
    """показываем профиль пользователя"""
    if(request.user.is_authenticated() and request.user.is_active):
        moder = request.user.isAllowedToModerate(request.session["town"], 'Profile')
        user = get_object_or_404(User, pk=user_id)
        allowed = (user.towns.all()[0] in [t for t in request.user.towns.all()] and moder)
        return render(request,'profile.html',{'allowed':allowed,'user':user})
    else:
        return redirect('/accounts/login')

def karma(request,user_id):
    """Листинг кармы пользователя"""
    from .helper import Karma
    if(request.user.is_authenticated() and request.user.is_active):
            try:
                page = request.GET.get('page', 1)
            except PageNotAnInteger:
                page = 1

            p = Paginator(Karma.list(get_object_or_404(User, pk=user_id)),10, request=request)
            points = p.page(page)
            return render(request,'karma_list.html',{'points': points})
    else:
        return redirect('/accounts/login')

def userban(request,user_id,status):
    if(request.user.is_authenticated() and request.user.is_active):
        user = get_object_or_404(User, pk=user_id)
        moder = request.user.isAllowedToModerate(request.session["town"], 'Profile')
        allowed = (user.towns.all()[0] in [t for t in request.user.towns.all()] and moder)
        if(allowed):
            user.is_active = int(status)
            user.save()
            return redirect('/profile/'+user_id)
        else:
            raise PermissionDenied("Доступ заборонено")
    else:
        raise PermissionDenied("Доступ заборонено")

#показываем пользователю после регистрации
def confirmyouremail(request):
    return render(request,'please_confirm_your_email.html')