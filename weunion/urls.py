
from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings
from . import views
from django.views.decorators.cache import cache_page

urlpatterns = [
    url(r'^$',views.index, name='index'),
    url(r'^admin_rm/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^defects/', include('defects.urls', namespace="defects")),
    url(r'^petitions/', include('petitions.urls', namespace="petitions")),
    url(r'^budget/', views.budget, name="budget"),
    url(r'^medicines/', include('medicines.urls', namespace="medicines")),
    url(r'^news/', include('news.urls', namespace="news")),
    url(r'^towns/+(?P<region_ref>[0-9]+)', views.towns, name="towns"),
    url(r'^town/+(?P<slug>[a-z]+)', views.index, name="index"),
    url(r'^choosetown/+(?P<townid>[0-9]+)', views.choosetown, name="choosetown"),
    url(r'^regions/', views.regions, name="regions"),
    url(r'^gettown', views.gettown, name="gettown"),
    url(r'^about', cache_page(60 * 1000)(views.about)),
    url(r'^help', views.help),
    url(r'^contacts',views.contacts),
    url(r'^join',views.join),
    url(r'^rules',views.rules),
    url(r'^test', views.test),
    url(r'^polls/', include('polls.urls', namespace='polls')),
    url(r'^confirmyouremail',views.confirmyouremail),
    url(r'^prozorro', views.prozorro, name="prozorro"),
    url(r'^igov',views.igov, name="igov"),
    url(r'^profile/karma/+(?P<user_id>[0-9]+)',views.karma, name="karma"),
    url(r'^profile/+(?P<user_id>[0-9]+)',views.profile, name="profile"),
    url(r'^profile/userban/+(?P<user_id>[0-9]+)/+(?P<status>[0-9]+)',views.userban, name="userban"),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes':False}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT2, 'show_indexes':False}),

]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
