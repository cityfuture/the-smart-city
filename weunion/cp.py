from .models import Town

def towntemp(request):

    town = None
    if("town" in request.session and "town_name" in request.session):
        town = request.session["town"]

    if(town):
        menu = Town.objects.get(pk=town).menu
        return {'town': True, 'menu':menu}
    else:
        return{'town':False}
