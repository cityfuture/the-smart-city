from django.conf import settings
from django.db import models
from weunion.models import Town


class Poll(models.Model):
    question = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    town = models.ForeignKey(Town, db_column='town_ref', related_name="town")
    date_start = models.DateField()
    date_end = models.DateField()
    active = models.BooleanField(default=True)
    archive = models.BooleanField(default=False)


    def count_choices(self):
        return self.choice_set.count()

    def count_total_votes(self):
        result = 0
        for choice in self.choice_set.all():
            result += choice.count_votes()
        return result

    def can_vote(self, user):
        if user:
            if(self.vote_set.filter(user=user).exists() or self.active == False or self.archive == True or (user.towns.all()[0] != self.town)):
                return False
            else:
                return True
        else:
            return False

    def __str__(self):
        return self.question


class Choice(models.Model):
    poll = models.ForeignKey(Poll)
    choice = models.CharField(max_length=255)

    def count_votes(self):
        return self.vote_set.count()

    def __unicode__(self):
        return self.choice

    class Meta:
        ordering = ['choice']


class Vote(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    poll = models.ForeignKey(Poll)
    choice = models.ForeignKey(Choice)

    def __unicode__(self):
        return u'Vote for %s' % (self.choice)

    class Meta:
        unique_together = (('user', 'poll'))
