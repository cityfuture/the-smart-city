from django.views.generic import DetailView, ListView, RedirectView
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages



from polls.models import Choice, Poll, Vote


class PollListView(ListView):

    def get_queryset(self):
        if 'town' in self.request.session:
            return Poll.objects.filter(active=1, town=self.request.session['town']).order_by('id','-archive')
        else:
            #return reverse_lazy('regions')
            return Poll.objects.filter(active=3)


class PollDetailView(DetailView):
    model = Poll

    def get_context_data(self, **kwargs):
        context = super(PollDetailView, self).get_context_data(**kwargs)

        if not(self.request.session.has_key('town')):
            self.request.session['town'] = self.object.town.id
            self.request.session['town_name'] = self.object.town.name

        if(self.request.user.is_authenticated() and self.request.user.is_active):
            context['poll'].votable = self.object.can_vote(self.request.user)
        else:
            context['poll'].votable = False
        return context


class PollVoteView(RedirectView):
    def post(self, request, *args, **kwargs):
        poll = Poll.objects.get(id=kwargs['pk'])
        user = request.user
        choice = Choice.objects.get(id=request.POST['choice_pk'])
        Vote.objects.create(poll=poll, user=user, choice=choice)
        messages.success(request,"Дякуємо за Ваш голос")
        return super(PollVoteView, self).post(request, *args, **kwargs)

    def get_redirect_url(self, **kwargs):
        return reverse_lazy('polls:detail', args=[kwargs['pk']])
