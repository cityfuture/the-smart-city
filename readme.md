### Document reference for modules used
```
http://django-debug-toolbar.readthedocs.org/en/1.4/
https://django-oauth-toolkit.readthedocs.org/en/latest/

```
## Jinja2 and django_jinja docs
```
http://jinja.pocoo.org/docs/dev/
http://niwinz.github.io/django-jinja/#_user_guide_for_django_1_8
```

## Pipeline
https://django-pipeline.readthedocs.org/en/latest/configuration.html

## Reports generator
http://django-report-builder.readthedocs.org/en/latest/howto/

##AllAuth
http://django-allauth.readthedocs.org/en/latest/overview.html  
http://www.sarahhagstrom.com/2013/09/the-missing-django-allauth-tutorial/  

##Design  
https://almsaeedstudio.com/themes/AdminLTE/index2.html

### Installation
```
sudo pip install -r requirements.txt
python manage.py migrate
python manage.py syncdb
python manage.py migrate oauth2_provider
python manage.py loaddata fixtures/defec

```
Then run the project 
```
#uwsgi --ini /etc/uwsgi/weunion-pub.ini
```
Stop project
```
#uwsgi --stop /tmp/master-weunion.pid
```
Log file
```
/var/log/uwsgi/weunion.log
```
Or for local instance
```
python manage.py runserver
```