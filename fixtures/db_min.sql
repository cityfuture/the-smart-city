-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.22-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица weunion.polls_choice
CREATE TABLE IF NOT EXISTS `polls_choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) NOT NULL,
  `choice` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `polls_choice_poll_id_72c5b9c8_fk_polls_poll_id` (`poll_id`),
  CONSTRAINT `polls_choice_poll_id_72c5b9c8_fk_polls_poll_id` FOREIGN KEY (`poll_id`) REFERENCES `polls_poll` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.polls_choice: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `polls_choice` DISABLE KEYS */;
INSERT INTO `polls_choice` (`id`, `poll_id`, `choice`) VALUES
	(7, 3, 'Нормально'),
	(8, 3, 'Херово');
/*!40000 ALTER TABLE `polls_choice` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.polls_poll
CREATE TABLE IF NOT EXISTS `polls_poll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `town_ref` int(11) NOT NULL DEFAULT '0',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `archive` int(1) DEFAULT '0',
  `question` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_polls_poll_town` (`town_ref`),
  CONSTRAINT `FK_polls_poll_town` FOREIGN KEY (`town_ref`) REFERENCES `town` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.polls_poll: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `polls_poll` DISABLE KEYS */;
INSERT INTO `polls_poll` (`id`, `town_ref`, `date_start`, `date_end`, `active`, `archive`, `question`, `description`) VALUES
	(3, 5, '2016-02-10', '2016-02-18', 1, 0, 'Ну як ви там?', 'Простеньке опитування');
/*!40000 ALTER TABLE `polls_poll` ENABLE KEYS */;


-- Дамп структуры для таблица weunion.polls_vote
CREATE TABLE IF NOT EXISTS `polls_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`poll_id`),
  KEY `polls_vote_choice_id_400bbb6_fk_polls_choice_id` (`choice_id`),
  KEY `polls_vote_poll_id_4e6e812c_fk_polls_poll_id` (`poll_id`),
  CONSTRAINT `polls_vote_choice_id_400bbb6_fk_polls_choice_id` FOREIGN KEY (`choice_id`) REFERENCES `polls_choice` (`id`) ON DELETE CASCADE,
  CONSTRAINT `polls_vote_poll_id_4e6e812c_fk_polls_poll_id` FOREIGN KEY (`poll_id`) REFERENCES `polls_poll` (`id`) ON DELETE CASCADE,
  CONSTRAINT `polls_vote_user_id_5f1aef7b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы weunion.polls_vote: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `polls_vote` DISABLE KEYS */;
/*!40000 ALTER TABLE `polls_vote` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
