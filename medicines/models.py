from django.db import models
from weunion.models import Town

class MedicinesDepartments(models.Model):
    hospital_ref = models.ForeignKey('MedicinesHospitals', db_column='hospital_ref')
    title = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'medicines_departments'


class MedicinesHospitals(models.Model):
    town_ref = models.ForeignKey(Town, db_column='town_ref')
    title = models.CharField(max_length=255)
    doctor = models.CharField(max_length=255)
    doctor_phone = models.CharField(max_length=20, blank=True, null=True)
    update_reestr = models.CharField(max_length=255, blank=True, null=True)
    responsible = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'medicines_hospitals'


class MedicinesItems(models.Model):
    department_ref = models.ForeignKey(MedicinesDepartments, db_column='department_ref')
    title = models.CharField(max_length=255)
    howmuch = models.CharField(max_length=25, blank=True, null=True)
    measure = models.CharField(max_length=25, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'medicines_items'

