from news.models import News
from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget

class NewsAdd(forms.ModelForm):
    class Meta:
        model = News
        fields = ['title','shortdesc','text', 'mainimg', 'publish']
        widgets = {
            'title': forms.TextInput(attrs={'placeholder': 'Уведіть заголовок новини. Не більше 150 символів', 'class': 'form-control input-lg'}, ),
            'shortdesc': forms.Textarea(attrs={'placeholder': 'Короткий текст, що передає контекст новини. Не більше 300 символів.', 'class': 'form-control input-lg',}),
            'text': CKEditorUploadingWidget(),
            'mainimg': forms.FileInput(attrs={'class':"filestyle",'data-buttonText': "Виберіть зображення", 'data-badge':"false",'data-buttonBefore':"true"}),

        }

